# -*- coding: utf-8 -*-
"""
Created on Tue Oct 10 18:51:02 2017

@author: Deepti
"""

import pylab as plt
import numpy as np
import pandas as pd

#K-means Function
def kMeans_generator(X, K, plot_progress = None):

    prevCentroids = [0]*3
    centroids = X[np.random.choice(np.arange(len(X)), K), :]
    while(np.array_equal(prevCentroids,centroids) == False):
        C = np.array([np.argmin([np.dot(xi-yk, xi-yk) for yk in centroids]) for xi in X])
        prevCentroids = centroids[:]
        centroids = [X[C == k].mean(axis = 0) for k in range(K)]
        if plot_progress != None: plot_progress(X, C, np.array(centroids))
    return np.array(centroids) , C

#Read file and create data frame
csv_reader = pd.read_csv("A:\\Fall 2017\\ML\\iris.csv", header=0)
df = pd.DataFrame(csv_reader)

#Convert the dataframe into a numpy array
data_matrix = df.as_matrix()
X = data_matrix[:,:3]

#Call to k-means function
centroid_matrix, C = kMeans_generator(X, K = 3, plot_progress = None)

#Plot 
plt.cla()
plt.plot(X[C == 0, 0], X[C == 0, 1], '*b',
        X[C == 1, 0], X[C == 1, 1], '*r',
        X[C == 2, 0], X[C == 2, 1], '*g')
plt.plot(centroid_matrix[:,0],centroid_matrix[:,1],'*m',markersize=20)
plt.draw()