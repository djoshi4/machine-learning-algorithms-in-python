# -*- coding: utf-8 -*-
"""
Created on Thu Dec 07 21:31:50 2017

@author: Deepti
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

#Reading input and getting the input matrix X
datafilereader = pd.read_csv("A:\Fall 2017\ML-HW2\SCLC_study_output_filtered_2.csv",header=0)
df = pd.DataFrame(datafilereader)
X = df.as_matrix()
X = X[:,1:].astype(float)

marker_size = 7

#Splitting input into 2 classes
class1 = X[:20,:]
class2 = X[20:,:]

#Calculate mean for both classes(1*19 mean vector)
mew_1 = class1.mean(axis=0)
mew_2 = class2.mean(axis=0)

#Create a 2D array with row1=mew_1 and row2=mew_2
mean_vector_matrix=np.array([mew_1,mew_2])


#Calculating S1 matrix
S1=np.zeros((19,19))
for i in range(20):
    diff=class1[i,:] - mew_1
    diff=np.array(diff)
    diff=diff.reshape((19,1))
    print i,"th row's diff is ", diff
    transpose_diff=diff.T
    S1 = S1 + (diff).dot(transpose_diff)

#Calculating S2 matrix
S2=np.zeros((19,19))
for i in range(20):
    diff=class2[i,:] - mew_2
    diff=np.array(diff)
    diff=diff.reshape((19,1))
    print i,"th row's diff is ", diff
    transpose_diff=diff.T
    S2 = S2 + (diff).dot(transpose_diff)


#Calculating Scatter within matrix
S_within = S1 + S2


#Calculating mean of entire class
overall_mean = np.mean(X, axis=0)


#Calculating scatter between
Scatter_between = np.zeros((19,19))
for i,mean_vec in enumerate(mean_vector_matrix):
    mean_vec = mean_vec.reshape(19,1) # make column vector
    overall_mean = overall_mean.reshape(19,1) # make column vector
    Scatter_between = np.add(Scatter_between,(20 * (mean_vec - overall_mean).dot((mean_vec - overall_mean).T)))


#Calculating scatter within inverse
S_within_inverse = np.linalg.inv(S_within)
print "S_within_inv ", S_within_inverse


#Calculating EigenValues and EigenVectors
eig_vals, eig_vecs = np.linalg.eig(np.dot(S_within_inverse,Scatter_between))


#Sorting in descending order
index = np.argsort(eig_vals)
index = index[::-1]
eig_vecs = eig_vecs[:,index]
eig_vals = eig_vals[index]


#Calculating Y matrix
Y = X.dot(eig_vecs[:,0])
print "Y: ", Y
Y=-Y


#Plotting the graph
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.set_title('My LDA Algorithm')
ax.set_xlabel(r'$W_1$')
ax.set_ylabel('')
ax.plot(Y[:20], np.zeros(20), linestyle='None', marker='o', markersize=marker_size, color='blue', label='NSCLC')
ax.plot(Y[20:], np.zeros(20), linestyle='None', marker='o', markersize=marker_size, color='red', label='SCLC')
ax.legend()
fig.show()