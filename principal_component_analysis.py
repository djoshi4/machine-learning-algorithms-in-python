# -*- coding: utf-8 -*-
"""
Created on Tue Sep 19 09:08:48 2017

@author: Deepti
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

#Read the csv file and store it in a data frame
csv_reader = pd.read_csv("A:\\Fall 2017\\ML-HW1\\linear_regression_test_data.csv", header=0)
data_matrix1 = pd.DataFrame(csv_reader)

#Convert the dataframe into a numpy array
inpmatrix = data_matrix1.as_matrix()
final_data_matrix = inpmatrix[:,1:3]

#Calculate mean centered
mean_val = final_data_matrix.mean(axis=0)
mean_centered = (final_data_matrix - mean_val)

#Calculating the covariance matrix. Using transpose of mean_centered matrix, 
#as cov function expects a random variable to be a row, but our mean_centered 
#matrix has them as columns. Thus, transposing it.

covariance_matrix=np.cov(mean_centered.astype(float).T)

#Finding eigenvalues and eigenvectors of covariance matrix
[eigenvalues,eigenvectors] = np.linalg.eig(covariance_matrix)

#Sorting eigenvalues and eigenvectors in descending order
index = np.argsort(eigenvalues)
index = index[::-1]
eigenvectors = eigenvectors[:,index]
eigenvalues = eigenvalues[index]

#Calculating the reduced matrix Y
Y=np.dot(final_data_matrix,eigenvectors)


# Plotting
fig = plt.figure()
ax = fig.add_subplot(1,1,1)
ax.set_title('y vs x, y-theoretical vs x, PC1 axis')
#Plot y-theoretical vs x
ax.scatter(inpmatrix[:,1],inpmatrix[:,3], color=['green'])
#Plot y vs x
ax.scatter(final_data_matrix[:,0],final_data_matrix[:,1], color=['blue'])
#Plot PC1 axis
k=3
ax.plot([0, (-1)*k*eigenvectors[0,0]], [0, (-1)*k*eigenvectors[1,0]],color='red', linewidth=3)
ax.set_xlabel('x')
ax.set_ylabel('y')
fig.show()