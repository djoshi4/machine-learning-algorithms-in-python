# -*- coding: utf-8 -*-
"""
Created on Fri Dec 08 18:27:30 2017

@author: Deepti
"""
import math
import numpy as np
import matplotlib.pyplot as plt


#----------------------------Forward propagation function------------------------
def forward(x,y,t_1,t_2):
    
    x = x.transpose()
    y = y.transpose()
    
    one_arr = np.array([[1]],dtype='float')
    
    a_1 = np.concatenate((one_arr,x),axis=0)
    z_2 = np.matmul(t_1,a_1)  
    a_2 = calculate_cost(z_2)
    z_3 = np.matmul(t_2,a_2)    
    a_3 = calculate_cost(z_3)
    a_3 = np.delete(a_3,0,0)
    
    tot = 0.5*((y[0]-a_3[0])**2 + (y[1]-a_3[1])**2)
    
    return a_1,a_2,a_3,tot

#----------------------------Cost calculation Function-------------------------------
def calculate_cost(z):
    x,y = z.shape
    e = math.e
    
    val_a = np.array([[1]], dtype='float')
    for i in range(0,x):
        val = 1/(1+e**z[i])
        val_a = np.concatenate((val_a,[val]),axis=0)
        
    return val_a

#---------------------------Derivative calculation Function------------------------
def calculate_derivative(z):
    return z * (1-z)

#------------------------------Back propagation Function---------------------------
def backward(a_1,a_2,a_3,t_1,t_2,y):
    
    alpha =0.5
    error_val = a_3 - y.T
    
    #Calculating delta_3
    delta_3 = error_val * calculate_derivative(a_3)
    
    m,n = a_2.shape
    a2_row = np.reshape(a_2,(n,m))
    new_t_2 = np.matmul(delta_3,a2_row) * alpha
        
    t2_1 = t_2[:,1]
    t2_1 = np.reshape(t2_1,(2,1))    
    a2_1_derivative = calculate_derivative(a_2[1])
    d2_1 = error_val * calculate_derivative(a_3) * t2_1 * a2_1_derivative
        
    t2_2 = t_2[:,2]
    t2_2 = np.reshape(t2_2,(2,1))
    a2_2_derivative = calculate_derivative(a_2[2])    
    d2_2 = error_val * calculate_derivative(a_3) * t2_2 * a2_2_derivative
    
    #Calculating delta_2
    delta_2 = np.zeros((2,1),dtype='float')
    delta_2[0,0] = d2_1.sum(axis=0)
    delta_2[1,0] = d2_2.sum(axis=0)
    
    #Getting dimensions of a1
    m,n = a_1.shape
    a1_row = np.reshape(a_1,(n,m))
    new_t_1 = np.matmul(delta_2,a1_row) * alpha
    
    new_t_1 = t_1 + new_t_1
    new_t_2 = t_2 + new_t_2
    
    return new_t_1,new_t_2,delta_3,delta_2

#--------------------------End of function definitions----------------------------


#-------------------------Given input and output---------------------------------
x = np.array([[0.05,0.1]],dtype='float')
y = np.array([[0.01,0.99]],dtype='float')
    
#----------------------------Initialization--------------------------------------
num_input_nodes = 2
num_hidden_nodes = 2
num_output_nodes = 2
np.random.seed(20)

total_cost = np.zeros((50000,2),dtype='float')
theta_first = np.zeros((50000,7),dtype='float')
theta_second = np.zeros((50000,7),dtype='float')
    

#Theta matrix Size: 2x3--> first column->Bias values; second and third column->Weights 

theta_1 = np.random.uniform(low=0.0,high=1.0,size=(num_hidden_nodes,num_input_nodes+1))
theta_2 = np.random.uniform(low=0.0,high=1.0,size=(num_output_nodes,num_hidden_nodes+1))
    
#----------------------------Starting Training--------------------------------------
for i in range(50000):    
#---------------------------Forward Propagation-------------------------------------
    a_1,a_2,a_3,tot = forward(x,y,theta_1,theta_2)
    total_cost[i,0]=i+1
    total_cost[i,1]=tot
    theta_first[i,0]=i+1
    theta_second[i,0]=i+1  
#Save values of theta1 in a matrix of 2*3
    theta_first[i,1]=theta_1[0,0]
    theta_first[i,2]=theta_1[0,1]
    theta_first[i,3]=theta_1[0,2]
    theta_first[i,4]=theta_1[1,0]
    theta_first[i,5]=theta_1[1,1]
    theta_first[i,6]=theta_1[1,2]
#Save values of theta2 in a matrix of 2*3
    theta_second[i,1]=theta_2[0,0]
    theta_second[i,2]=theta_2[0,1]
    theta_second[i,3]=theta_2[0,2]
    theta_second[i,4]=theta_2[1,0]
    theta_second[i,5]=theta_2[1,1]
    theta_second[i,6]=theta_2[1,2]

    
#-----------------------Backward Propagation-------------------------------------
    new_t_1, new_t_2, delta_3, delta_2 = backward(a_1,a_2,a_3,theta_1,theta_2,y)
	#Saving theta1 and theta2
    theta_1 = new_t_1
    theta_2 = new_t_2
 
    
#-----------------------Graph Plotting-------------------------------------------       

#---------------------------------------THETA 1---------------------------------------------------

#Bias weight from input to hidden layer
fig = plt.figure()
plot1 = fig.add_subplot(1, 1, 1)
plot1.scatter(theta_first[:,0], theta_first[:,1], color='red',s=10, label='theta1-1 vs iterations') 
plot1.legend()
fig.show()
    
#Weight to hidden layer
fig = plt.figure()
plot2 = fig.add_subplot(1, 1, 1)
plot2.scatter(theta_first[:,0], theta_first[:,2], color='red',s=10, label='theta1-2 vs iterations') 
plot2.legend()
fig.show()
    
#Weight to output layer
fig = plt.figure()
plot3 = fig.add_subplot(1, 1, 1)
plot3.scatter(theta_first[:,0], theta_first[:,3], color='red',s=10, label='theta1-3 vs iterations') 
plot3.legend()
fig.show()
    
#Bias weight from input to hidden layer
fig = plt.figure()
plot4 = fig.add_subplot(1, 1, 1)
plot4.scatter(theta_first[:,0], theta_first[:,4], color='red',s=10, label='theta1-4 vs iterations') 
plot4.legend()
fig.show()
    
#Weight to hidden layer
fig = plt.figure()
plot5 = fig.add_subplot(1, 1, 1)
plot5.scatter(theta_first[:,0], theta_first[:,5], color='red',s=10, label='theta1-5 vs iterations') 
plot5.legend()
fig.show()
 
#Weight to output layer   
fig = plt.figure()
plot6 = fig.add_subplot(1, 1, 1)
plot6.scatter(theta_first[:,0], theta_first[:,6], color='red',s=10, label='theta1-6 vs iterations') 
plot6.legend()
fig.show()      
        
#--------------------------------------THETA 2-----------------------------------------------------------

#Bias weight from hidden to output layer
fig = plt.figure()
plot1 = fig.add_subplot(1, 1, 1)
plot1.scatter(theta_second[:,0], theta_second[:,1], color='green',s=10, label='theta2-1 vs iterations') 
plot1.legend()
fig.show()
    
#Weight to hidden layer
fig = plt.figure()
plot2 = fig.add_subplot(1, 1, 1)
plot2.scatter(theta_second[:,0], theta_second[:,2], color='green',s=10, label='theta2-2 vs iterations') 
plot2.legend()
fig.show()
    
#Weight to output layer
fig = plt.figure()
plot3 = fig.add_subplot(1, 1, 1)
plot3.scatter(theta_second[:,0], theta_second[:,3], color='green',s=10, label='theta2-3 vs iterations') 
plot3.legend()
fig.show()
    
#Bias weight from hidden to output layer
fig = plt.figure()
plot4 = fig.add_subplot(1, 1, 1)
plot4.scatter(theta_second[:,0], theta_second[:,4], color='green',s=10, label='theta2-4 vs iterations') 
plot4.legend()
fig.show()

#Weight to hidden layer
fig = plt.figure()
plot5 = fig.add_subplot(1, 1, 1)
plot5.scatter(theta_second[:,0], theta_second[:,5], color='green',s=10, label='theta2-5 vs iterations') 
plot5.legend()
fig.show()

#Weight to output layer
fig = plt.figure()
plot6 = fig.add_subplot(1, 1, 1)
plot6.scatter(theta_second[:,0], theta_second[:,6], color='green', s=10, label='theta2-6 vs iterations') 
plot6.legend()
fig.show()    
    
# Total Cost vs Iterations
fig = plt.figure()
plot1 = fig.add_subplot(1, 1, 1)
plot1.scatter(total_cost[:,0], total_cost[:,1], color='blue', s=10, label='Total Cost vs iterations') 
plot1.legend()
fig.show() 