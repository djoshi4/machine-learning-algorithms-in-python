# -*- coding: utf-8 -*-
"""
Created on Thu Nov 22 15:12:40 2017

@author: Deepti
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

 
def find_coefficients(x, y):
    
    # Find the mean of x and y vector
    mean_x = np.mean(x)
    mean_y = np.mean(y)
    # Get size of x
    num = np.size(x)
    xy = np.sum(y*x - num*mean_y*mean_x)
    xx = np.sum(x*x - num*mean_x*mean_x)
 
    # Finding b0 and b1
    b1 = xy / xx
    b0 = mean_y - b1*mean_x
 
    return(b0, b1)
 
 
def main():
    
#    Read the csv file and store it in a data frame
    csv_reader = pd.read_csv("A:\\Fall 2017\\ML-HW1\\linear_regression_test_data.csv", header=0)
    data_matrix1 = pd.DataFrame(csv_reader)

    #Convert the dataframe into a numpy array
    inpmatrix = data_matrix1.as_matrix()
    final_data_matrix = inpmatrix[:,1:3]
    x=final_data_matrix[:,0].T
    y=final_data_matrix[:,1].T
    print "x: ", x
    print "y: ", y
    print final_data_matrix    
    
    #Find the coefficients
    b = find_coefficients(x, y)
    print("Coefficients:\nb0 = {}  \
          \nb1 = {}".format(b[0], b[1]))
 
    # Plotting linear regression line
    plt.scatter(x, y, color = "b", marker = "o", s = 40)
    y_predicted = b[0] + b[1]*x
    plt.plot(x, y_predicted, color = "r")
    plt.xlabel('x')
    plt.ylabel('y') 
    plt.show()
 
if __name__ == "__main__":
    main()