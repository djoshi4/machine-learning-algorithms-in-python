# -*- coding: utf-8 -*-
"""
Created on Tue Dec 12 00:47:29 2017

@author: Deepti
"""
import numpy as np
from sklearn.datasets import load_iris
import matplotlib.pyplot as plt

    
#----------------------------------Given input--------------------------------------
iris = load_iris()
data = iris.data

target = iris.target
target = target.reshape((150,1))
data = data[50:,:]
target = target[50:]

features = iris.feature_names

#Scaling the data
minimum  = data.min(axis=0)
maximum = data.max(axis=0)
scaled = (data - minimum)/(maximum - minimum)
scaled_temp=scaled[:,2:4]
X=np.concatenate((np.ones((scaled_temp.shape[0],1)),scaled_temp),axis=1)
y = target
y=y-1


#------------------------------Initialization-------------------------------------
trainX = X[:99,:]
testX = X[99:]
trainY = y[:99,:]
testY = y[99:]
    

m,n = trainX.shape
error = 0
alpha = 0.1
num_of_iterations = 500

#----------------------------------Start Training loop----------------------------
for itern in range(100):
    print "Iteration number:",itern
    np.random.seed(itern)
    delta = 0
    J = np.zeros(num_of_iterations)
    theta = np.zeros((num_of_iterations,n))
    trainY
    for idx in range(num_of_iterations-1):
        delta = np.zeros((n,1))
        for t in range(99):
            X = np.transpose(trainX[t,:])
            cur_z = np.sum(theta[idx,:] * X)
            hat_x = 1.0 / (1.0 + np.exp(-cur_z))
            res = hat_x - trainY[t]
            res = np.reshape(res,(1,1))
            delta = delta + np.matmul(np.reshape(X,(3,1)),res)
            Cost =-((trainY[t] * np.log10(hat_x)) + ((1.0-trainY[t]) * np.log10(1.0-hat_x)))
            J[idx] = J[idx]+Cost

        
        J[idx] = J[idx]/float(99)    
        theta[idx+1,:] = theta[idx,:] - alpha * delta.T / float(99)
    
    for t in range(99):
        X = np.transpose(trainX[t,:])
        cur_z = sum(theta[num_of_iterations-1,:] * X)
        hat_x = 1.0 / (1.0 + np.exp(-cur_z))
        Cost = -((trainY[t] * np.log10(hat_x)) + ((1.0-trainY[t]) * np.log10(1.0-hat_x)))
        J[num_of_iterations-1] = J[num_of_iterations-1]+Cost
    J[num_of_iterations-1] = J[num_of_iterations-1] / float(99)
    
    
#---------------------------------Prediction------------------------------------------
    final_theta = theta[-1,:]
    final_theta = final_theta.reshape((len(final_theta),1))
    z_predict = np.matmul(testX,final_theta)
    y_predict_prob = 1.0 / (1.0 + np.exp(-z_predict))
    
    y_predict_binary = np.zeros(len(y_predict_prob))
    for i in range(len(y_predict_prob)):
        if y_predict_prob[i] >= 0.5:
            y_predict_binary[i] = 1.0
        else:
            y_predict_binary[i] = 0.0
    print "Output : ",y_predict_prob 
    if(y_predict_binary == testY):
        error=error + 0
    else:
        error=error + 1
        
    trainX = np.concatenate((testX,trainX),axis=0)
    trainY = np.concatenate((testY,trainY),axis=0)
    testX = np.copy(trainX[99:,:])
    testY = np.copy(trainY[99:,:])
    trainX = np.delete(trainX,99,0)
    trainY = np.delete(trainY,99,0)
    

print "Error rate found: ", error, "/100 !!" 