# -*- coding: utf-8 -*-
"""
Created on Mon Dec 11 20:42:53 2017

@author: Deepti
"""

import numpy as np
import matplotlib.pyplot as plt
from sklearn.datasets import load_iris

#------------------------------Back propagation Function-----------------------------
def backward(a_1,a_2,a_3,t_1,t_2,y):
    
    error_val = -(np.dot(y,(1/a_3)) + np.dot((1-y),(-1/(1-a_3))))
    
    #Calculating delta_3
    delta_3 = error_val * calculate_derivative(a_3)

    m,n = a_2.shape    
    a2_row = np.reshape(a_2,(n,m))
    new_t_2 = np.matmul(delta_3,a2_row)

        
    t2_1 = t_2[:,1]
    t2_1 = np.reshape(t2_1,(1,1))    
    a2_1_derivative = calculate_derivative(a_2[1])
    d2_1 = error_val * calculate_derivative(a_3) * t2_1 * a2_1_derivative
        
    t2_2 = t_2[:,2]
    t2_2 = np.reshape(t2_2,(1,1))
    a2_2_derivative = calculate_derivative(a_2[2])    
    d2_2 = error_val * calculate_derivative(a_3) * t2_2 * a2_2_derivative
    
    #Calculating delta_2
    delta_2 = np.zeros((2,1),dtype='float')
    delta_2[0,0] = d2_1.sum(axis=0)
    delta_2[1,0] = d2_2.sum(axis=0)
    
    m,n = a_1.shape
    a1_row = np.reshape(a_1,(n,m))
    new_t_1 = np.matmul(delta_2,a1_row)
       
    return new_t_1,new_t_2,delta_3,delta_2

#----------------------------Cost calculation Function-------------------------------
def calculate_cost(z):
    return (1.0 / (1.0 + np.exp(-z))) 

#---------------------------Derivative calculation Function--------------------------
def calculate_derivative(z):
    return z * (1-z)

#--------------------------End of function definitions------------------------------

#-------------------------Given input and output------------------------------------

iris = load_iris()
data = iris.data

target = iris.target
target = target.reshape((150,1))
data = data[50:,:]
target = target[50:]

features = iris.feature_names

#Scaling the data
minimum  = data.min(axis=0)
maximum = data.max(axis=0)
scaled = (data - minimum)/(maximum - minimum)

X = scaled[:,2:4]
y = target
y=y-1

    
#----------------------------Initialization-----------------------------------------
num_input_nodes = 2
num_hidden_nodes = 2
num_output_nodes = 1

trainX = X[:99,:]
testX = X[99:]
trainY = y[:99,:]
testY = y[99:]

error = 0
alpha = 0.5

#Training set loop
for itern in range(100):
    np.random.seed(itern)
    theta_1 = np.random.uniform(low=0.0,high=1.0,size=(num_hidden_nodes,num_input_nodes+1))
    theta_2 = np.random.uniform(low=0.0,high=1.0,size=(num_output_nodes,num_hidden_nodes+1))
        
#----------------------------Starting Training---------------------------------------

    for idx in range(500):           
        Total = 0
        gradient_theta_1 = np.zeros(theta_1.shape)
        gradient_theta_2 = np.zeros(theta_2.shape)
        
#---------------------------Forward Propagation--------------------------------------
        for trn in range(99):

            a_1 = np.concatenate(([1],np.transpose(trainX[trn])),axis=0).reshape(3,1)
            z_2 = np.matmul(theta_1, a_1)
            a_2 = calculate_cost(z_2)
            a_2 = np.concatenate(([[1]],a_2),axis=0).reshape(3,1)
            z_3 = np.matmul(theta_2,a_2)
            a_3 = calculate_cost(z_3)
            Cost = -((trainY[trn] * np.log10(a_3)) + ((1-trainY[trn]) * np.log10(1-a_3)))
            Total += Cost
    
#-----------------------Backward Propagation-----------------------------------------
            new_t_1, new_t_2, delta_3, delta_2 = backward(a_1,a_2,a_3,theta_1,theta_2,trainY[trn])
            #Saving theta1 and theta2
            gradient_theta_1 += new_t_1
            gradient_theta_2 += new_t_2
                    
        #Dataset loop ends    
        derivative_cost_1 = gradient_theta_1 / float(99)
        derivative_cost_2 = gradient_theta_2 / float(99)
        theta_1 = theta_1 - alpha * derivative_cost_1
        theta_2 = theta_2 - alpha * derivative_cost_2
                
    #Convergence loop ends 
    print "------------------------------------------------------------"
    print "iteration number: ", itern 
    print "theta1 value: ",theta_1
    print "theta2 value: ",theta_2
    
#-------------------------------Testing----------------------------------------------

    a_1 = np.concatenate(([[1]],np.transpose(testX)),axis=0).reshape(3,1)
    z_2 = np.matmul(theta_1, a_1)
    a_2 = calculate_cost(z_2)
    a_2 = np.concatenate(([[1]],a_2),axis=0).reshape(3,1)
    z_3 = np.matmul(theta_2,a_2)
    a_3 = calculate_cost(z_3)
    #Classifying the error as 0 or 1
    if(a_3[0]<=0.5):
        op = 0       
    else:
        op = 1
    if(op==testY[0]):
         error = error + 0
    else:
         error = error + 1
    
    #Deleting the last element and putting it on top of TrainX/Y, and taking the next last element as test
    trainX = np.concatenate((testX,trainX),axis=0)
    trainY = np.concatenate((testY,trainY),axis=0)
    testX = np.copy(trainX[99:,:])
    testY = np.copy(trainY[99:,:])
    trainX = np.delete(trainX,99,0)
    trainY = np.delete(trainY,99,0) 
    print "a_3: ", a_3 
    
#Initial loop ends   
print "Error rate found: ", error, "/100 !!"